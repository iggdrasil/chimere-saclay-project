<?php echo('<?xml version="1.0" encoding="UTF-8"?>'); ?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<?php include('lib/functions.php'); ?>
<html>
<head>
<title>Carte OuVerte du plateau de Saclay</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
<link href="css/home.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

var initialized = false;
$(function(){
    $('.ok').click(function(){
       $('#main_form').submit();
    });
    $('input[name="courrel"]').click(function(){
        if (!initialized){
            initialized = true;
            $("input[name='courrel']").val('');
        }
    });
<?php

if($_POST['courrel']){
$ip = $_SERVER['REMOTE_ADDR'];
if(!checkIP($ip)){
    echo('alert("Trop de d\'adresses de courriel ont été enregistrées depuis votre réseau. Veuillez renouveller votre demande un autre jour. Merci.");');
} else if(checkEmail($_POST['courrel'])){
    addEmail($_POST['courrel'], $ip);
    echo('alert("Votre adresse de courriel '.$_POST['courrel'].' a bien été enregistrée. Merci de votre intérêt ! Nous vous tiendrons informé dans les prochains jours.");');
}else{
    echo('alert("L\'adresse '.$_POST['courrel'].' est incorrecte. Elle a déjà été enregistrée ou ce n\'est pas une adresse valide.");');
};

}?>
});
//-->
</script>
</head>

<body onLoad="MM_preloadImages('img/telecharger_on.png','img/savoir_on.png','img/dorian_on.png','img/lea_on.png','img/sabine_on.png')">
<div id="new_body">
<div id="header">
<form action="." method="post" id='main_form'><input name="courrel" type="text" class="box" value="adresse de courriel"><img src="img/ok.png" class="ok"></form></div>
<div id="link"><a href="Carte-ouverte_Saclay-presentation.pdf" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image2','','img/telecharger_on.png',1)"><img src="img/telecharger_off.png" name="Image2" width="599" height="30" border="0"></a></div>
<div id="link"><a href="http://bit.ly/SmQpga" target="_blank" onMouseOver="MM_swapImage('Image3','','img/savoir_on.png',1)" onMouseOut="MM_swapImgRestore()"><img src="img/savoir_off.png" name="Image3" width="599" height="30" border="0"></a></div>
<div id="img"><img src="img/logo.png"></div>
  <div id="link2"><a href="mailto:dorian.spaak@terreetcite.org" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image4','','img/dorian_on.png',1)"><img src="img/dorian_off.png" name="Image4" width="599" height="20" border="0"></a></div>
  <div id="link2"><a href="mailto:leamarzloff@groupechronos.org" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image5','','img/lea_on.png',1)"><img src="img/lea_off.png" name="Image5" width="599" height="20" border="0"></a></div>
  <div id="link2"><a href="mailto:sdarmaillacq@gmail.com" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image6','','img/sabine_on.png',1)"><img src="img/sabine_off.png" name="Image6" width="599" height="20" border="0"></a></div>
<div id="img"><img src="img/footer.png"></div>
</div>
</body>
</html>
