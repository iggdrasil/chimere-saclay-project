$(function(){
    $('#default-message').dialog({'autoOpen':false,
                                  'resizable':false,
                                  width:630,
                                  'dialogClass':'no-titlebar'});

    var update_editmarker = function(){
        $("#main-map").chimere('cleanRoute');
        $('.map_button').hide();
        if($('#multimedia_form').length) $('#multimedia_form').remove();
        $('#action-edit-event').removeClass('state-active');
        $('#action-edit-route').removeClass('state-active');
        $('#action-edit-marker').addClass('state-active');
        $('#frm-edit-route').hide();
        $('#frm-edit-event').hide();
        $('#frm-edit-marker').show();
        $.ajax({url: edit_url,
                dataType: "html",
                success: function (data) {
                    $('#frm-edit-event').html('');
                    $('#frm-edit-route').html('');
                    $('#frm-edit-marker').html(data);
                    $("#main-map").chimere('activateMarkerEdit');
                },
                error: function (data) {
                    $('#frm-edit-marker').html("<p class='warning'>"+
                                        default_error_message+"</p>");
                }
            });
    };

    var update_editevent = function(){
        $("#main-map").chimere('cleanRoute');
        $('.map_button').hide();
        if($('#multimedia_form').length) $('#multimedia_form').remove();
        $('#action-edit-marker').removeClass('state-active');
        $('#action-edit-route').removeClass('state-active');
        $('#action-edit-event').addClass('state-active');
        $('#frm-edit-marker').hide();
        $('#frm-edit-route').hide();
        $('#frm-edit-event').show();
        $.ajax({url: edit_event_url,
                dataType: "html",
                success: function (data) {
                    $('#frm-edit-marker').html('');
                    $('#frm-edit-route').html('');
                    $('#frm-edit-event').html(data);
                    $("#main-map").chimere('activateMarkerEdit');
                },
                error: function (data) {
                    $('#frm-edit-event').html("<p class='warning'>"+
                                        default_error_message+"</p>");
                }
            });
    };

    var update_editroute = function(){
        $('.map_button').show();
        if($('#multimedia_form').length) $('#multimedia_form').remove();
        $('#action-edit-marker').removeClass('state-active');
        $('#action-edit-event').removeClass('state-active');
        $('#action-edit-route').addClass('state-active');
        $('#frm-edit-marker').hide();
        $('#frm-edit-event').hide();
        $('#frm-edit-route').show();
        $('#chimere_itinerary').hide();
        $('#chimere_itinerary_form').show();
        $.ajax({url: edit_route_url,
                dataType: "html",
                success: function (data) {
                    $('#frm-edit-marker').html('');
                    $('#frm-edit-event').html('');
                    $('#frm-edit-route').html(data);
                    $("#main-map").chimere('cleanMarker');
                    $("#main-map").chimere('activateRouteEdit');
                },
                error: function (data) {
                    $('#frm-edit-route').html("<p class='warning'>"+
                                        default_error_message+"</p>");
                }
            });
    };
    $("#action-news").click(function(){
        $('#news').dialog('open');
    });
    $("#action-carte").click(function(){
        $('.map_button').hide();
        $("#main-map").chimere('cleanRoute');
        $("#main-map").chimere('cleanMarker');
        $("#main-map").chimere('activateContextMenu');
        if($("#itinerary_field").html()){
            $("#chimere_itinerary_form").appendTo("#chimere_itinerary_panel");
            if($("#chimere_itinerary").css('display') != 'none'){
                $("#chimere_itinerary_form").hide();
            }
        }
        $('#action-participate').removeClass('state-active');
        $('#action-carte').addClass('state-active');
        $('#edit-actions').hide();
        $('#map-actions').show();
        $('#edit-panel').hide();
        $('#map-panel').show();
    });

    $("#action-participate").click(function(){
        $('.map_button').hide();
        $('#action-carte').removeClass('state-active');
        $('#action-participate').addClass('state-active');
        $('#map-actions').hide();
        $('#edit-actions').show();
        $('#map-panel').hide();
        $('#edit-panel').show();
        update_editmarker();
    });

    $("#action-edit-event").click(update_editevent);
    $("#action-edit-marker").click(update_editmarker);
    $("#action-edit-route").click(update_editroute);

    $('.dyn-page').click(function(){
        var url = $(this).filter('a').attr('href');
        if (!url){
            url = $(this).children('a').attr('href');
        }
        if (!url){
            return false;
        }
        $.ajax({url: url,
                dataType: "html",
                success: function (content) {
                    html = "<div class='dialog-content'>" +
                           content + "</div>"
                    $('#default-message').html(html);
                    $('#default-message').dialog('open');
                }
            });
        return false;
    });

    $('.print-page').click(function(){
        window.print();
        return false;
    });
    $('.extra-criteria .extra-label').click(function(){
        var par = $(this).parent();
        var form = par.children('.extra-form');
        form.toggle();
        if(form.is(':visible')){
            par.addClass('opened');
        } else {
            par.removeClass('opened');
        }
    });
    $('input[name="transport"]').change(change_routing_transport);
    $('input[name="speed"]').change(change_routing_speed);
    $('#id_speed').change(change_routing_speed);
});

function change_routing_speed(){
    var speed = $('#id_speed option:selected').val();
    if(!speed){
       speed = $('input[name=speed]:checked').val();
    }
    if (!speed) return;
    speed = speed.split('_')[1]
    $('#main-map').chimere('routingChangeSpeed', speed);
}

function change_routing_transport(){
    $('#main-map').chimere('routingChangeTransport',
                           $('input[name="transport"]:checked').val());
}

function display_feature_detail(data, settings){
    settings.current_popup.setContentHTML("<div class='cloud'>" + data + "</div>");
    $('.detail_zoomin').bind("click", function(event){
        $('#main-map').chimere('hidePopup', event);
        $('#main-map').chimere('zoomIn');
    });
    $('.detail_zoomout').bind("click", function(event){
        $('#main-map').chimere('hidePopup', event);
        $('#main-map').chimere('zoomOut');
    });
    $('.detail_from').bind("click", function(event){
        $('#main-map').chimere('routingFrom');
        $('#main-map').chimere('hidePopup', event);
    });
    $('.detail_step').bind("click", function(event){
        $('#main-map').chimere('routingAddStep');
        $('#main-map').chimere('hidePopup', event);
    });
    $('.detail_to').bind("click", function(event){
        $('#main-map').chimere('routingTo');
        $('#main-map').chimere('hidePopup', event);
    });
    jQuery(".close_img").click(function(event){
        $("#main-map").chimere('hidePopup', event);
    });
    jQuery("#read_more_"+settings.current_feature.pk).click(function(){
        $('#detail_content').css('height', $('#detail_content').height());
        $("#description_short_"+settings.current_feature.pk).hide();
        $("#description_long_"+settings.current_feature.pk).show();
        $("#read_more_"+settings.current_feature.pk).hide();
        $("#read_less_"+settings.current_feature.pk).show();
    });
    jQuery("#read_less_"+settings.current_feature.pk).click(function(){
        $("#description_long_"+settings.current_feature.pk).hide();
        $("#description_short_"+settings.current_feature.pk).show();
        $("#read_less_"+settings.current_feature.pk).hide();
        $("#read_more_"+settings.current_feature.pk).show();
    });
    $(document).ready(function(){
        share_link_update();
        $("a[rel^='prettyPhoto']").prettyPhoto({
            show_title: false,
            social_tools: ''
        });
    });
    $('html').addClass('js-on');
    $(function(){
        $('div.media-player').jmeEmbedControls();
        setTimeout(function(){
            settings.current_popup.updateSize();
            setTimeout(function(){ settings.current_popup.updateSize(); }, 1000);
        }, 1000);
    });
}

function toggleDrawOn() {
    $('#button-move-map').removeClass('toggle-button-active'
                        ).addClass('toggle-button-inactive');
    $('#button-draw-map').removeClass('toggle-button-inactive'
                        ).addClass('toggle-button-active');
    $("#main-map").chimere("activateCurrentControl");
}

function toggleDrawOff() {
    $('#button-draw-map').removeClass('toggle-button-active'
                        ).addClass('toggle-button-inactive');
    $('#button-move-map').removeClass('toggle-button-inactive'
                        ).addClass('toggle-button-active');
    $("#main-map").chimere("deactivateCurrentControl");
}
