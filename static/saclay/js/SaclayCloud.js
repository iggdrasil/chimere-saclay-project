
/* set this empty if you put cloud-popup-saclay.png inside your default
 * OpenLayers dir */
var SACLAY_IMAGE_PATH = '../../saclay/images/'

OpenLayers.Popup.SaclayCloud =
  OpenLayers.Class(OpenLayers.Popup.FramedCloud, {

    initialize:function(id, lonlat, contentSize, contentHTML, anchor, closeBox,
                        closeBoxCallback) {
        this.positionBlocks.tc = {
            'offset': new OpenLayers.Pixel(-217, 28),
            'padding': new OpenLayers.Bounds(8, 30, 8, 9),
            'blocks': [
                { // top-left
                    size: new OpenLayers.Size('auto', 'auto'),
                    anchor: new OpenLayers.Bounds(0, 51, 22, 0),
                    position: new OpenLayers.Pixel(0, 0)
                },
                { //top-right
                    size: new OpenLayers.Size(22, 'auto'),
                    anchor: new OpenLayers.Bounds(null, 50, 0, 0),
                    position: new OpenLayers.Pixel(-1238, 0)
                },
                { //bottom-left
                    size: new OpenLayers.Size('auto', 19),
                    anchor: new OpenLayers.Bounds(0, 32, 22, null),
                    position: new OpenLayers.Pixel(0, -631)
                },
                { //bottom-right
                    size: new OpenLayers.Size(22, 18),
                    anchor: new OpenLayers.Bounds(null, 32, 0, null),
                    position: new OpenLayers.Pixel(-1238, -631)
                    /*anchor: new OpenLayers.Bounds(null, 32, 10, null),
                    position: new OpenLayers.Pixel(-1228, -632)*/
                },
                { // stem
                    size: new OpenLayers.Size(54, 35),
                    anchor: new OpenLayers.Bounds(null, 0, 168, null),
                    position: new OpenLayers.Pixel(0, -688)
                }
            ]
        };

        this.calculateRelativePosition = function () {return 'tc';};
        this.autoSize = true;
        this.imageSrc = (OpenLayers.Util.getImageLocation(SACLAY_IMAGE_PATH + 'cloud-popup-saclay.png'));
        OpenLayers.Popup.Framed.prototype.initialize.apply(this, arguments);
        this.contentDiv.className = this.contentDisplayClass;
    },

    CLASS_NAME: "OpenLayers.Popup.SaclayCloud"
});

