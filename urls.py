#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.conf import settings
from django.conf.urls.defaults import *

from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

admin.autodiscover()
urlpatterns = patterns('django.views.static',
    (r'^%s(?P<path>.*)' % settings.MEDIA_URL[1:], 'serve',
     {'document_root': settings.MEDIA_ROOT}),
)
urlpatterns += staticfiles_urlpatterns()

urlpatterns += patterns('',
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),
    (r'^admin/', include(admin.site.urls)),
    url(r'^(?P<area_name>[a-zA-Z0-9_-]+/)?edit/$', 'chimere_saclay.views.edit',
        name="edit"),
    url(r'^(?P<area_name>[a-zA-Z0-9_-]+/)?edit/(?P<item_id>\w+)/(?P<submited>\w+)?$',
            'chimere_saclay.views.edit', name="edit-item"),
    url(r'^(?P<area_name>[a-zA-Z0-9_-]+/)?edit-event/$',
        'chimere_saclay.views.edit', name="edit-event", kwargs={'event':True}),
    url(r'^(?P<area_name>[a-zA-Z0-9_-]+/)?edit-event/(?P<item_id>\w+)/(?P<submited>\w+)?$',
        'chimere_saclay.views.edit', kwargs={'event':True},
        name="edit-event-item"),
    url(r'^(?P<area_name>[a-zA-Z0-9_-]+/)?edit-route/$',
        'chimere_saclay.views.edit_route', name="edit-route"),
    url(r'^(?P<area_name>[a-zA-Z0-9_-]+/)?edit-route/(?P<item_id>\w+)/(?P<submited>\w+)?$',
        'chimere_saclay.views.edit_route', name="edit-route-item"),
    url(r'^(saclay/)?$', 'chimere_saclay.views.index_saclay',
        name="index-saclay"),
    url(r'^', include('chimere.urls', namespace="chimere")),
)


