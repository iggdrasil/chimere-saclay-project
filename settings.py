#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Don't edit this file:
# overload all theses settings in your local_settings.py file

import os
import sys
_ = lambda s: s

DEBUG = False
TEMPLATE_DEBUG = DEBUG
MOBILE_TEST = False
DJANGO_EXTENSIONS = False

# Django settings for chimere project.
PROJECT_NAME = u'Chimère'
ROOT_PATH = os.path.realpath(os.path.dirname(__file__)) + "/"

EMAIL_HOST = 'localhost'
CONTACT_EMAIL = ''
STATIC_URL = '/static/'
STATIC_ROOT = ROOT_PATH + 'static/'

TINYMCE_URL = '/tinymce/'
EXTRA_CSS = []
JQUERY_JS_URLS = []
JQUERY_CSS_URLS = []

OSM_CSS_URLS = ["http://www.openlayers.org/api/theme/default/style.css"]

GPSBABEL = '/usr/bin/gpsbabel'
# simplify with an  error of 5 meters
GPSBABEL_OPTIONS = 'simplify,crosstrack,error=0.005k'
# GPSBABEL_OPTIONS = 'simplify,count=100'

# chimere specific ##
CHIMERE_DEFAULT_ZOOM = 10
# center of the map
CHIMERE_DEFAULT_CENTER = (-1.679444, 48.114722)
# projection used by the main map
# most public map providers use spherical mercator : 900913
CHIMERE_EPSG_PROJECTION = 900913
# projection displayed to the end user by openlayers
# chimere use the same projection to save its data in the database
CHIMERE_EPSG_DISPLAY_PROJECTION = 4326
# display of shortcuts for areas
CHIMERE_DISPLAY_AREAS = True
# number of day before an event to display
# if equal to 0: disable event management
# if you change this value from 0 to a value in a production environnement
# don't forget to run the upgrade.py script to create appropriate fields in
# the database
CHIMERE_DAYS_BEFORE_EVENT = 30
# Dated events must usualy be checked as 'front page' to be displayed
# on front page - set CHIMERE_ALL_DATED_ARE_FRONT to True if you want to
# display all events on front page
CHIMERE_ALL_DATED_ARE_FRONT = True
# allow feeds
CHIMERE_FEEDS = True

CHIMERE_ICON_WIDTH = 21
CHIMERE_ICON_HEIGHT = 25
CHIMERE_ICON_OFFSET_X = -10
CHIMERE_ICON_OFFSET_Y = -25


# display picture inside the description by default or inside a galery?
CHIMERE_MINIATURE_BY_DEFAULT = False

# JS definition of the default map (for admin and when no map are defined in
# the application)
# cf. OpenLayers documentation for more details

# OSM mapnik map
CHIMERE_DEFAULT_MAP_LAYER = "new OpenLayers.Layer.OSM.Mapnik('Mapnik')"

CHIMERE_XAPI_URL = 'http://open.mapquestapi.com/xapi/api/0.6/'
CHIMERE_OSM_API_URL = 'api06.dev.openstreetmap.org'  # test URL
CHIMERE_OSM_USER = 'test'
CHIMERE_OSM_PASSWORD = 'test'

# encoding for shapefile import
CHIMERE_SHAPEFILE_ENCODING = 'ISO-8859-1'

# as the web server need to be reloaded when property models are changed
# it could be a good idea to hide it to an admin who could'nt do that
CHIMERE_HIDE_PROPERTYMODEL = False

CHIMERE_ENABLE_CLUSTERING = False

# enable routing in Chimère
CHIMERE_ENABLE_ROUTING = False

CHIMERE_ROUTING_TRANSPORT = (('foot', _(u"Foot")),
                             ('bicycle', _(u"Bicycle")),
                             ('motorcar', _(u"Motorcar")),
                             )

CHIMERE_ROUTING_SPEEDS = {'foot': ((3, _(u"You are walking slowly")),
                                   (6, _(u"You are walking pretty quickly")),),
                          'bicycle': ((16, _(u"You are riding pretty slowly")),
                                      (22, _(u"You are riding pretty quickly"))
                                      )
                          }

# available routing engine: 'routino'
CHIMERE_ROUTING_ENGINE = {
    'ENGINE': 'routino',
    'PATH': '/usr/local/src/web/bin/router',
    'DB_PATH': '/var/local/routino/',
}

CHIMERE_ROUTING_FAIL_MESSAGE = u"""<h3 class='warn'>Attention</h3>
<p>Le moteur de routage a échoué dans sa recherche de trajet. Les points de
départ ou d'arrivée sont peut-être trop loin d'une voie existante ou le trajet
est trop dangereux.</p>"""

NOMINATIM_URL = 'http://nominatim.openstreetmap.org/search'

# thumbnail
CHIMERE_THUMBS_SCALE_HEIGHT = 250
CHIMERE_THUMBS_SCALE_WIDTH = None

# length of short description
CHIMERE_SHORT_DESC_LENGTH = 400

CHIMERE_MODIF_EMAIL = _(u"Hello, I would like to propose you a modification "
                        u"about this item: ")

CHIMERE_ROUTING_WARN_MESSAGE = u"<h3 class='warn'>Attention</h3>"\
    u"<p>Cet itinéraire comporte des passages dangereux, nous vous conseillons"\
    u" de modifier votre recherche, en ajoutant par exemple un ou des points "\
    u"d'étape à votre parcours pour éviter les zones de danger.</p>"

CHIMERE_CSV_ENCODING = 'ISO-8859-1'

# generic contact email
CONTACT_EMAIL = ''

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'NAME': 'ratatouille',
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'HOST': 'localhost',
        'PORT': '5432',
        'USER': 'ratatouille',
        'PASSWORD': 'wiki',
    },
}

if 'test' in sys.argv:
    SOUTH_TESTS_MIGRATE = False

# Local time zone for this installation. Choices can be found here:
# http://www.postgresql.org/docs/8.1/static/datetime-keywords.html#DATETIME-TIMEZONE-SET-TABLE
# although not all variations may be possible on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes
# http://blogs.law.harvard.edu/tech/stories/storyReader$15
LANGUAGE_CODE = 'fr-FR'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True
USE_L10N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = ROOT_PATH + 'media/'

# URL that handles the media served from MEDIA_ROOT.
# Example: "http://media.lawrence.com"
MEDIA_URL = '/media/'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #     'django.template.loaders.eggs.load_template_source',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.doc.XViewMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware'
)

ROOT_URLCONF = 'chimere_saclay.urls'

TEMPLATE_DIRS = [
    ROOT_PATH + 'templates',
]

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request",
)

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.gis',
    'django.contrib.staticfiles',
]

# celery
try:
    import djcelery
    import kombu
    kombu
    djcelery.setup_loader()
    BROKER_URL = 'django://'
    INSTALLED_APPS += ['kombu.transport.django',
                       'djcelery']
except ImportError:
    # some import and export will not be available
    pass

INSTALLED_APPS += [
    'south',
    'chimere',
]

MOBILE_DOMAINS = []  # if you have specific domains for mobile access

LOGFILENAME = '/var/log/django/chimere.log'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        # Include the default Django email handler for errors
        # This is what you'd get without configuring logging at all.
        'mail_admins': {
            'class': 'django.utils.log.AdminEmailHandler',
            'level': 'ERROR',
            # But the emails are plain text by default - HTML is nicer
            'include_html': True,
        },
        # Log to a text file that can be rotated by logrotate
        'logfile': {
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': '/var/log/django/chimere.log'
        },
    },
    'loggers': {
        # Again, default Django configuration to email unhandled exceptions
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        # Might as well log any errors anywhere else in Django
        'django': {
            'handlers': ['logfile'],
            'level': 'ERROR',
            'propagate': False,
        },
        # Your own app - this assumes all your logger names start with "myapp."
        'chimere': {
            'handlers': ['logfile'],
            'level': 'WARNING',  # Or maybe INFO or DEBUG
            'propogate': False
        },
    },
}

try:
    from local_settings import *
except ImportError, e:
    print 'Unable to load local_settings.py:', e

if DJANGO_EXTENSIONS:
    INSTALLED_APPS.append('django_extensions')

if not JQUERY_JS_URLS:
    JQUERY_JS_URLS = (STATIC_URL + 'jquery/jquery.min.js',
                      STATIC_URL + 'jquery-ui/jquery-ui.min.js')

if not JQUERY_CSS_URLS:
    JQUERY_CSS_URLS = (STATIC_URL + 'jquery-ui/smoothness/jquery-ui.css',
                       STATIC_URL + 'jquery-ui/base/jquery.ui.all.css')


LOGGING['handlers']['logfile']['filename'] = LOGFILENAME

if 'CHIMERE_SHARE_NETWORKS' not in globals():
    # after the locals to get the right STATIC_URL

    # share with
    global CHIMERE_SHARE_NETWORKS
    CHIMERE_SHARE_NETWORKS = (
        ("Email", 'mailto:?subject=%(text)s&body=%(url)s',
         STATIC_URL + 'chimere/img/email.png'),
        ("Facebook", 'http://www.facebook.com/sharer.php?t=%(text)s&u=%(url)s',
         STATIC_URL + 'chimere/img/facebook.png'),
        ("Twitter", 'http://twitter.com/home?status=%(text)s %(url)s',
         STATIC_URL + 'chimere/img/twitter.png'),
    )

if 'OSM_JS_URLS' not in globals():
    global OSM_JS_URLS
    OSM_JS_URLS = [STATIC_URL + "openlayers/OpenLayers.js",
                   STATIC_URL + "openlayers/SimplePanZoom.js",
                   "http://www.openstreetmap.org/openlayers/OpenStreetMap.js"]
if 'OSM_MOBILE_JS_URLS' not in globals():
    global OSM_MOBILE_JS_URLS
    OSM_MOBILE_JS_URLS = [
        STATIC_URL + "openlayers/OpenLayers.mobile.js",
        "http://www.openstreetmap.org/openlayers/OpenStreetMap.js"
    ]
