#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.conf import settings
from django.shortcuts import render_to_response
from django.template import RequestContext

from forms import MarkerForm, RouteForm

from chimere.actions import actions
from chimere.models import Marker, Route, PropertyModel
from chimere.views import get_edit_page, index

def index_saclay(request, area_name=None, default_area=None, simple=False):
    tpl, response_dct = index(request, area_name, default_area, simple,
                              get_response=True)
    response_dct['news_visible'] = False if request.GET else True
    response_dct['simple'] = simple
    return render_to_response(tpl, response_dct,
                              context_instance=RequestContext(request))

get_edit_marker = get_edit_page('edit', Marker, MarkerForm)

def edit(request, area_name="", item_id=None, submited=False, event=False):
    """
    Edition page
    """
    response, values, sub_categories = get_edit_marker(request, area_name,
                                                       item_id, ['M', 'B'])
    if response:
        return response
    item_id, init_item, response_dct, form, formset_multi, formset_picture = \
                                                                        values
    # get the "manualy" declared_fields. Ie: properties
    declared_fields = form.declared_fields.keys()
    declared_fields = PropertyModel.objects.filter(available=True).all()
    filtered_properties = PropertyModel.objects.filter(available=True,
                                subcategories__id__isnull=False).all()
    response_dct.update({
        'actions':actions,
        'action_selected':('contribute', 'edit'),
        'map_layer':settings.CHIMERE_DEFAULT_MAP_LAYER,
        'form':form,
        'formset_multi':formset_multi,
        'formset_picture':formset_picture,
        'dated':settings.CHIMERE_DAYS_BEFORE_EVENT,
        'extra_head':form.media,
        'marker_id':item_id,
        'sub_categories':sub_categories,
        'point_widget':'',
        'properties':declared_fields,
        'filtered_properties':filtered_properties,
        'submited':submited,
        'event':event,
    })
    # manualy populate the custom widget
    if 'subcategory' in form.data and form.data['subcategory']:
        response_dct['current_category'] = int(form.data['subcategory'])
    return render_to_response('chimere/edit.html', response_dct,
                              context_instance=RequestContext(request))

get_edit_route = get_edit_page('edit-route', Route, RouteForm)

def edit_route(request, area_name="", item_id=None, submited=False):
    """
    Edition page
    """
    response, values, sub_categories = get_edit_route(request, area_name,
                                                       item_id, ['R', 'B'])
    if response:
        return response
    item_id, init_item, response_dct, form, formset_multi, formset_picture = \
                                                                        values
    # get the "manualy" declared_fields. Ie: properties
    declared_fields = form.declared_fields.keys()
    declared_fields = PropertyModel.objects.filter(available=True).all()
    filtered_properties = PropertyModel.objects.filter(available=True,
                                subcategories__id__isnull=False).all()
    response_dct.update({
        'actions':actions,
        'action_selected':('contribute', 'edit'),
        'map_layer':settings.CHIMERE_DEFAULT_MAP_LAYER,
        'form':form,
        'formset_multi':formset_multi,
        'formset_picture':formset_picture,
        'dated':settings.CHIMERE_DAYS_BEFORE_EVENT,
        'extra_head':form.media,
        'marker_id':item_id,
        'sub_categories':sub_categories,
        'point_widget':'',
        'properties':declared_fields,
        'filtered_properties':filtered_properties,
        'submited':submited,
        'route':True
    })
    # manualy populate the custom widget
    if 'subcategory' in form.data and form.data['subcategory']:
        response_dct['current_category'] = int(form.data['subcategory'])
    return render_to_response('chimere/edit.html', response_dct,
                              context_instance=RequestContext(request))
